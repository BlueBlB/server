package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.alexi.awbd.dtos.CustomerDTO;
import ro.alexi.awbd.model.Customer;


@Mapper(componentModel = "spring", uses = {})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(target = "addresses", source = "addresses")
    Customer toEntity(CustomerDTO addressDTO);

    @Mapping(target = "addresses", source = "addresses")
    CustomerDTO toDto(Customer address);
}
