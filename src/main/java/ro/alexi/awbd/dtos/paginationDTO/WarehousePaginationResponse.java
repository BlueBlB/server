package ro.alexi.awbd.dtos.paginationDTO;



import ro.alexi.awbd.dtos.WarehouseDTO;

import java.util.List;


public class WarehousePaginationResponse {
    private List<WarehouseDTO> warehouses;
    private long totalItems;


    public List<WarehouseDTO> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<WarehouseDTO> warehouses) {
        this.warehouses = warehouses;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }
}
