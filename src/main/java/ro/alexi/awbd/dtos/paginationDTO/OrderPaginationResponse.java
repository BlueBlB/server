package ro.alexi.awbd.dtos.paginationDTO;

import ro.alexi.awbd.dtos.OrderDTO;

import java.util.List;

public class OrderPaginationResponse {
    private List<OrderDTO> orders;
    private long totalItems;

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }
}
