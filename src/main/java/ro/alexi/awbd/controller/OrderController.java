package ro.alexi.awbd.controller;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.OrderDTO;
import ro.alexi.awbd.dtos.paginationDTO.OrderPaginationResponse;
import ro.alexi.awbd.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/awbd/order")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/placeOrder")
    public ResponseEntity<OrderDTO> placeOrder(@RequestBody OrderDTO order) {

        OrderDTO orderDTO = orderService.placeOrder(order);

        return new ResponseEntity<>(orderDTO, HttpStatus.ACCEPTED);

    }

    @GetMapping("")
    public ResponseEntity<List<OrderDTO>> getOrders(@RequestParam int pageNumber, @RequestParam int pageSize) {

        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("orderNumber").ascending());

        OrderPaginationResponse orders = orderService.getOrders(pageable);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(orders.getTotalItems()));

        return new ResponseEntity<>(orders.getOrders(), headers, HttpStatus.OK);

    }
}
