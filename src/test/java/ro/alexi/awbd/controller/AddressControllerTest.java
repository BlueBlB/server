package ro.alexi.awbd.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import ro.alexi.awbd.AwbdApplication;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.security.SecurityUserService;
import ro.alexi.awbd.service.AddressService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AwbdApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@WithUserDetails(value="admin@admin.com", userDetailsServiceBeanName="securityUserService")
class AddressControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    AddressService addressService;

    @Autowired
    SecurityUserService securityUserService;


    @BeforeEach
    void setUp() {

    }

    @Test
    void getAllAddresses() throws Exception {

        mvc.perform(get("/awbd/address/list")
                .param("pageNumber", "0")
                .param("pageSize", "10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}